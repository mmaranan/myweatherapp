# WeatherApp

An Android MVP weather application that uses the OpenWeatherMap API (http://openweathermap.org).

## Libraries

The libraries and tools used include:

- Support library
- CardViews
- Retrofit
- Realm
- Butterknife(https://github.com/JakeWharton/butterknife)